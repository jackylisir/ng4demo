import { HostListener, Directive, ElementRef, OnInit } from '@angular/core';

declare var $ : any;
@Directive({
  selector: '[app-nav-collapse]'
})
export class NavCollapseDirective implements OnInit {

  el : any;
  dropdownMenus: any;
  notDropdownMenus: any;
  subMenus: any;
  app: any;

  constructor(private elRef: ElementRef) {
    console.log('init directive');
    this.el = elRef.nativeElement;
  }

  @HostListener('mouseenter') onMouseEnter() {
  }

  ngOnInit() {
    this.app = $('body');
    this.dropdownMenus = $('>li.dropdown',this.el);
    this.notDropdownMenus = $(this.el).children('li').not(this.dropdownMenus);
    this.subMenus = $('>ul>li', this.dropdownMenus);
    this.notDropdownMenus.on('click', (e) => {
      let menu = e.currentTarget;
      $(menu).addClass('active');
      $(this.dropdownMenus).removeClass('active open');
      $('>ul', this.dropdownMenus).css('display', 'none');
    });
    this.dropdownMenus.on('click', (e) => {
      if($(this.app).hasClass('sidebar-sm') || $(this.app).hasClass('sidebar-xs')) return;
      let menu = e.currentTarget;
      if($(menu).hasClass('open')) {
        $(menu).removeClass('open');
        $(">ul", menu).css('display', 'none');
      } else {
        $(menu).addClass('open');
        $(">ul", menu).css('display', 'block');
      }
    });
    this.subMenus.on('click', (e) => {
      let subMenu = e.currentTarget;
      $(this.subMenus).removeClass('active');
      $(subMenu).addClass('active');
      this.diactiveAllMenuItems();
      $(subMenu).parent().parent().addClass('active');
      e.preventDefault();
      e.stopPropagation();
    });
  }

  diactiveAllMenuItems() {
    $(this.dropdownMenus).removeClass('active');
    $(this.notDropdownMenus).removeClass('active');
  }

  recoverAllSubMenuItems() {

  }

  activeMenuItem(menu) {

  }

  activeSubMenuItem(subMenu) {

  }

}
