import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login.component";
import { FormsModule } from "@angular/forms";
import { BusyModule, BusyConfig } from 'angular2-busy';
const routes:Routes = [
    {   
        path:'auth', 
        children:[
            {path:'login', component:LoginComponent}
        ]
    },
]
@NgModule({
    imports:[RouterModule, RouterModule.forChild(routes), 
        CommonModule,
        FormsModule, 
        BusyModule,
        ],
    exports:[RouterModule],
    declarations:[LoginComponent],
    providers: [
        { provide: BusyConfig, useFactory: busyConfigFactory }
    ]
})
export class LoginModule {

}

export function busyConfigFactory() {
  return new BusyConfig({
      message: 'Don\'t panic!',
      backdrop: false,
      // template: '<div class="spinner"></div>',
      template: '<div class="sk-fading-circle"> <div class="sk-circle1 sk-circle"></div> <div class="sk-circle2 sk-circle"></div> <div class="sk-circle3 sk-circle"></div> <div class="sk-circle4 sk-circle"></div> <div class="sk-circle5 sk-circle"></div> <div class="sk-circle6 sk-circle"></div> <div class="sk-circle7 sk-circle"></div> <div class="sk-circle8 sk-circle"></div> <div class="sk-circle9 sk-circle"></div> <div class="sk-circle10 sk-circle"></div> <div class="sk-circle11 sk-circle"></div> <div class="sk-circle12 sk-circle"></div> </div>',
      delay: 200,
      minDuration: 600,
      wrapperClass: 'global-indicator'
  });
}