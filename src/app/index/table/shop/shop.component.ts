import { Http } from "@angular/http";
import { Component, OnInit } from '@angular/core';
import { MeiyuDataSource } from "../datasource/meiyu-data-source";
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  busy : any;
  source : MeiyuDataSource;
  settings = {
    pager: {
      perPage: 10
    },
    actions: {
      edit: true, //as an example
      delete: true,
      add: false,
      columnTitle: "操作",
      custom: [
        { name: 'search', title: `<i class="fa fa-search fa-2x"></i> ` },
        { name: 'delete', title: `<i class="fa fa-remove fa-2x"></i> ` },
        { name: 'edit', title: `<i class="fa fa-edit fa-2x"></i> ` },
      ],
      position: 'right',
    },
    attr: {
      class: 'table table-hover table-striped'
    },
    hideSubHeader: true,
    columns: {
      id: {
        title: '编号',
      },
      brand: {
        title: '品牌',
        width: '20%',
      },
      shopName: {
        title: '店铺名称',
      },
      shopStar: {
        title: '评分',
      },
      createdAt: {
        title: '抓取时间',
        valuePrepareFunction: (timestamp) => {
          console.log(timestamp);
          return 'ok'
        }
      },
    }
  };

  constructor(http: Http) { 
    this.source = new MeiyuDataSource(http, {
      endPoint: environment.endPoint + '/shops',
      totalKey: 'totalElements',
      dataKey: 'content',
      pagerPageKey: 'page',
      pagerLimitKey: 'size',
      sortFieldKey: 'sort',
    })
  }


  ngOnInit() {
    this.source.loading().subscribe((promise) => {
      this.busy = promise;
    })
  }
}
