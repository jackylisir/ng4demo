import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TestDirective } from './page-not-found/test.directive';
import { MenuItemDirective } from './page-not-found/menu-item.directive';
import { SubMenuItemDirective } from './page-not-found/sub-menu-item.directive';
export const routes: Routes = [
    {
        path: '404',
        component: PageNotFoundComponent
    }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PageNotFoundComponent, TestDirective, MenuItemDirective, SubMenuItemDirective]
})
export class ErrorModule { }
