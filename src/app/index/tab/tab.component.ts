import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  tabs = [
    {label: "aaaa", template: "<i fa fa-close></i>"},
    {label: "aaaa", template: "<i fa fa-close></i>"},
    {label: "aaaa", template: "<i fa fa-close></i>"},
  ]
  selectedIndex: number;
  constructor() { }

  ngOnInit() {
  }

  onTabChange() {
    console.log('tab changed. %d', this.selectedIndex);
    
    
  }

}
