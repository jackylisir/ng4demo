import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormControl } from "@angular/forms";
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  loading = false;
  chart1: any;
  optionDataSet: any;
  options = [
    '1111',
    '1bbbbb',
    '1ccccc',
    '2aaaa',
  ]
   filteredOptions: string[];
  myControl = new FormControl();
  chartOption = {
    title: {
      text: '堆叠区域图'
    },
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        boundaryGap: false,
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: '邮件营销',
        type: 'line',
        stack: '总量',
        areaStyle: { normal: {} },
        data: [120, 132, 101, 134, 90, 230, 210]
      },
      {
        name: '联盟广告',
        type: 'line',
        stack: '总量',
        areaStyle: { normal: {} },
        data: [220, 182, 191, 234, 290, 330, 310]
      },
      {
        name: '视频广告',
        type: 'line',
        stack: '总量',
        areaStyle: { normal: {} },
        data: [150, 232, 201, 154, 190, 330, 410]
      },
      {
        name: '直接访问',
        type: 'line',
        stack: '总量',
        areaStyle: { normal: {} },
        data: [320, 332, 301, 334, 390, 330, 320]
      },
      {
        name: '搜索引擎',
        type: 'line',
        stack: '总量',
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        areaStyle: { normal: {} },
        data: [820, 932, 901, 934, 1290, 1330, 1320]
      }
    ]
  }

  barOption = {
    title: [
      {
        text: 'ECharts 入门示例',
        left: 'center'
      },
    ],

    // tooltip: {},
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      orient: 'horizontal',
      right: '10%',
      data: ['销量', '总量']
    },
    xAxis: {
      data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"],

    },
    grid: {
      right: '30%',
      top: '15%',
      containLabel: true

    },
    yAxis: {},
    series: [
      {
        name: '销量',
        type: 'bar',
        stack: 'xxx',
        data: [8,12,3,12,6,3]
      },
      {
        name: '总量',
        type: 'bar',
        stack: 'xxx',
        data: [1,3,5,8,12,3]
      }, 
      {
        type: 'pie',
        formatter:'{d} xx',
        // legend: ['aa', 'bb', 'cc'],
        center: ['82%', '23%'],
        radius: ['20%', '30%'],
        tooltip: {
          trigger: 'item',
          formatter: '{a} : {b} : {c} {d}'
        },
        label: {
          normal: {
            formatter: '{d}',
            textStyle: {
              color: 'green'
            }
          },
          emphasis: {
            formatter: '{d}',
          }

        },
        data: [13,25,18],
      },
      {
        type: 'pie',
        center: ['82%', '73%'],
        radius: ['20%', '30%'],
        data: [13,25,18],
      }
    ]
  };

  pieOptions = {
    tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    series: [
      {
        name: '访问来源',
        type: 'pie',
        radius: '55%',
        data: [
          { value: 235, name: '视频广告' },
          { value: 274, name: '联盟广告' },
          { value: 310, name: '邮件营销' },
          { value: 335, name: '直接访问' },
          { value: 400, name: '搜索引擎' }
        ],
        // label: {
        //     normal: {
        //         textStyle: {
        //             color: 'rgba(255, 255, 255, 0.3)'
        //         }
        //     }
        // },
        // labelLine: {
        //     normal: {
        //         lineStyle: {
        //             color: 'rgba(255, 255, 255, 0.3)'
        //         }
        //     }
        // },
        // itemStyle: {
        //     normal: {
        //         color: '#c23531',
        //         shadowBlur: 200,
        //         shadowColor: 'rgba(0, 0, 0, 0.5)'
        //     }
        // }
      },
    ]
  }
  map1Option = {
    title: [
      { text: '地图' },
    ],
    // tooltip: {
    //     trigger: 'item',
    //     formatter: '{b}'
    // },
    series: [
      {
        name: '中国',
        type: 'map',
        mapType: 'beijing',
        map: 'beijing',
        // selectedMode : 'multiple',
        // label: {
        //     normal: {
        //         show: true
        //     },
        //     emphasis: {
        //         show: true
        //     }
        // },
        // data:[
        //     {name:'广东', selected:true}
        // ]
      }
    ]
  };

  mapOption = {
    title: {
      text: 'iphone销量',
      subtext: '纯属虚构',
      left: 'center'
    },
    tooltip: {
      trigger: 'item',
      // formatter: '{a}<br>{b}<br>{c1}'
    },
    legend: {
      orient: 'vertical',
      left: 'right',
      top: 30,
      data: ['iphone3', 'iphone4', 'iphone5']
    },
       xAxis: {
        type: 'value',
        scale: true,
        position: 'top',
        boundaryGap: false,
        splitLine: {
            show: false
        },
        axisLine: {
            show: false
        },
        axisTick: {
            show: false
        },
        axisLabel: {
            margin: 2,
            textStyle: {
                color: 'green'
            }
        },
          data:[3,19,2,4]
    },
    yAxis: {
        type: 'category',
        //  name: 'TOP 20',
        nameGap: 16,
        axisLine: {
            show: true,
            lineStyle: {
                color: 'red'
            }
        },
        axisTick: {
            show: false,
            lineStyle: {
                color: 'green'
            }
        },
        axisLabel: {
            interval: 0,
            textStyle: {
                color: 'blue'
            }
        },
        data: ['aa','bb', 'cc', 'dd']
    },
    visualMap: {
      min: 0,
      max: 2500,
      left: 'left',
      top: 'bottom',
      text: ['高', '低'],           // 文本，默认为数值文本
      calculable: true
    },
    // toolbox: {
    //   show: true,
    //   // orient: 'vertical',
    //   left: 'right',
    //   feature: {
    //     dataView: { readOnly: false },
    //     restore: {},
    //     saveAsImage: {}
    //   }
    // },
    series: [
      {
        selectedMode : 'multiple',
        name: 'iphone3',
        type: 'map',
        mapType: 'china',
        roam: false,
        label: {
          normal: {
            show: true
          },
          emphasis: {
            show: true
          }
        },
        data: [
          { name: '北京', value: 500 },
          { name: '天津', value: this.randomData() },
          { name: '上海', value: this.randomData() },
          { name: '重庆', value: this.randomData() },
          { name: '河北', value: this.randomData() },
          { name: '河南', value: this.randomData() },
          { name: '云南', value: this.randomData() },
          { name: '辽宁', value: this.randomData() },
          { name: '黑龙江', value: this.randomData() },
          { name: '湖南', value: this.randomData() },
          { name: '安徽', value: this.randomData() },
          { name: '山东', value: this.randomData() },
          { name: '新疆', value: this.randomData() },
          { name: '江苏', value: this.randomData() },
          { name: '浙江', value: this.randomData() },
          { name: '江西', value: this.randomData() },
          { name: '湖北', value: this.randomData() },
          { name: '广西', value: this.randomData() },
          { name: '甘肃', value: this.randomData() },
          { name: '山西', value: this.randomData() },
          { name: '内蒙古', value: this.randomData() },
          { name: '陕西', value: this.randomData() },
          { name: '吉林', value: this.randomData() },
          { name: '福建', value: this.randomData() },
          { name: '贵州', value: this.randomData() },
          { name: '广东', value: this.randomData() },
          { name: '青海', value: this.randomData() },
          { name: '西藏', value: this.randomData() },
          { name: '四川', value: this.randomData() },
          { name: '宁夏', value: this.randomData() },
          { name: '海南', value: this.randomData() },
          { name: '台湾', value: this.randomData() },
          { name: '香港', value: this.randomData() },
          { name: '澳门', value: this.randomData() }
        ]
      },
      {
        name: 'iphone4',
        type: 'map',
        mapType: 'china',
        label: {
          normal: {
            show: true
          },
          emphasis: {
            show: true
          }
        },
        data: [
          { name: '北京', value: 300 },
          { name: '天津', value: this.randomData() },
          { name: '上海', value: this.randomData() },
          { name: '重庆', value: this.randomData() },
          { name: '河北', value: this.randomData() },
          { name: '安徽', value: this.randomData() },
          { name: '新疆', value: this.randomData() },
          { name: '浙江', value: this.randomData() },
          { name: '江西', value: this.randomData() },
          { name: '山西', value: this.randomData() },
          { name: '内蒙古', value: this.randomData() },
          { name: '吉林', value: this.randomData() },
          { name: '福建', value: this.randomData() },
          { name: '广东', value: this.randomData() },
          { name: '西藏', value: this.randomData() },
          { name: '四川', value: this.randomData() },
          { name: '宁夏', value: this.randomData() },
          { name: '香港', value: this.randomData() },
          { name: '澳门', value: this.randomData() }
        ]
      },
      {
        name: 'iphone5',
        type: 'map',
        mapType: 'china',
        label: {
          normal: {
            show: true
          },
          emphasis: {
            show: true
          }
        },
        data: [
          { name: '北京', value: 200 },
          { name: '天津', value: this.randomData() },
          { name: '上海', value: this.randomData() },
          { name: '广东', value: this.randomData() },
          { name: '台湾', value: this.randomData() },
          { name: '香港', value: this.randomData() },
          { name: '澳门', value: this.randomData() }
        ]
      }
    ]
  }

  constructor() { }

  ngOnInit() {
    console.log(this.myControl);
    // this.filteredOptions = 
    this.myControl.valueChanges.subscribe((v) => {
      console.log(v);
      this.filteredOptions = this.options.filter(option => new RegExp(`^${v}`, 'gi').test(option)); 

    });
      // .startWith(null)
      // .map(val => val ? this.filter(val) : this.options.slice());
  }
  filter(val: string): string[] {
    console.log('++++++++==');
      return this.options.filter(option => new RegExp(`^${val}`, 'gi').test(option)); 
  }

  changeLoading() {
    this.loading = !this.loading;
    this.optionDataSet = [
      [{name: '袜子', value: 21}, 2, 3, 14],
      [5, 2, 3, 4],
    ]
    // this.option.series[0].data = [15, 22, 23, 8, 10, 12];

  }

  randomData() {
    return Math.round(Math.random() * 1000);
  }

  onChartClick(event) {
    console.log(event.name);
    console.log(event.value);
  }
  onChartInit(chart) {
    this.chart1 = chart;
      console.log('listen');
      console.log(chart);
      this.chart1.on('mapselectchanged',(params) => {
        console.log(params);
      });
      
  }
  onSelectChanged(x) {
    console.log('onSelectChanged');
    console.log(x);
    
    

  }
}
