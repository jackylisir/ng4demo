import { ContentChildren, Component, OnInit, HostBinding, HostListener, QueryList, ViewChildren, AfterViewInit } from '@angular/core';
import { MenuItemDirective } from "./menu-item.directive";

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent implements OnInit {

  @ViewChildren(MenuItemDirective) menus: QueryList<MenuItemDirective>;
  constructor() { }

  ngAfterContentInit() {
    console.log('after content init');
  }
  ngAfterViewInit() {
    console.log(this.menus);
  }
  ngOnInit() {
    console.log('init page not found component');
  }

  clickBtn() {
    console.log("click btn");
  }

  @HostListener('mousedown') hasPressed() {
    return true;
  }
}
