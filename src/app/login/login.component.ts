import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";

import { AuthService } from "../service/auth.service";
import { GlobalStateService } from "../service/global-state.service";

@Component({
    selector:'login',
    templateUrl:'login.html',
    styleUrls:['login.scss']
})

export class LoginComponent implements OnInit {
    model : any = {};
    busy : any;

    constructor(
        private authService: AuthService,
        private globalState: GlobalStateService,
        private http: Http,
        private router: Router
    ) {}

    ngOnInit() : void {
        console.log('init login component1');
        this.authService.loginProcess().subscribe((promise) => {
            this.busy = promise;
        })
    }

    login() {
        this.authService.login(this.model.username, this.model.password).subscribe((user)=> {
            this.router.navigate(['/index/blank']);
        }, (e) => {
        })
    }


    register(): void {
        console.log(this.globalState.token);
    }


}