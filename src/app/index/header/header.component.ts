import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service'

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  switchRightBar() {
    if($(document.body).hasClass('rightbar-show')) {
      $(document.body).removeClass('rightbar-show').addClass('rightbar-hidden');
    } else {
      $(document.body).removeClass('rightbar-hidden').addClass('rightbar-show');
    }
  }

  switchSideBarMode(e) {
    let o = e.currentTarget;
    console.log(o);
    
    let body = $(document.body);
    if(body.hasClass('sidebar-sm')) {
      $('>i', o).removeClass('fa-indent').addClass('fa-outdent');
      body.removeClass('sidebar-sm');
    } else {
      body.addClass('sidebar-sm');
      $('>i', o).removeClass('fa-outdent').addClass('fa-indent');
    }
  }
  logout() {
    this.authService.logout();
  }

}
