import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

import { CrisisCenterComponent} from "./crisis-center.component";
import { CrisisDetailComponent} from "./crisis-detail.component";
import { CrisisCenterHomeComponent } from "./crisis-center-home.component";
import { CrisisListComponent } from "./crisis-list.component";
import { AuthGuardService } from "../service/auth-guard.service";

export const routes: Routes = [
    {
        path: 'crisis',
        component: CrisisCenterComponent,
        canActivate:[AuthGuardService],
        children:[
            {
                path:'',
                component: CrisisListComponent,
                children: [
                    {
                        path: ':id',
                        component: CrisisDetailComponent
                    },
                    {
                        path: '',
                        component: CrisisCenterHomeComponent
                    }
                ]
            },
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
