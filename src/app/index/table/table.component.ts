import { Component, OnInit } from '@angular/core';
import { MyLoginComponent } from "../components/my-login/my-login.component";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  settings = {
    actions: {
      edit: false, //as an example
      delete: false,
      add: false,
      custom: [
        { name: 'search', title: `<i class="fa fa-search fa-2x"></i> ` },
        { name: 'delete', title: `<i class="fa fa-remove fa-2x"></i> ` },
        { name: 'edit', title: `<i class="fa fa-edit fa-2x"></i> ` },
      ],
      width: 300,
      position: 'right',
    },
    attr: {
      class: 'table table-hover table-striped'
    },
    hideSubHeader: true,
    columns: {
      id: {
        title: 'ID',
      },
      name: {
        title: 'Full Name',
      },
      username: {
        title: 'User Name',
        sort: true

      },
      email: {
        title: 'Email'
      },
      button: {
      title: 'Button',
      type: 'custom',
      renderComponent: MyLoginComponent,
      onComponentInitFunction(instance) {
        // instance.save.subscribe(row => {
        //   alert(`${row.name} saved!`)
        // });
        }
      }
    }
  };
  data = [
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "Sincere@april.biz",
      button: '#1'


    },
    {
      id: 2,
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv",
      button: '#2'
    },

    // ... list of items

    {
      id: 11,
      name: "Nicholas DuBuque",
      username: "Nicholas.Stanton",
      email: "Rey.Padberg@rosamond.biz",
      button: '#3'
    },
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "Sincere@april.biz",
      button: '#1'


    },
    {
      id: 2,
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv",
      button: '#2'
    },

    // ... list of items

    {
      id: 11,
      name: "Nicholas DuBuque",
      username: "Nicholas.Stanton",
      email: "Rey.Padberg@rosamond.biz",
      button: '#3'
    },
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "Sincere@april.biz",
      button: '#1'


    },
    {
      id: 2,
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv",
      button: '#2'
    },

    // ... list of items

    {
      id: 11,
      name: "Nicholas DuBuque",
      username: "Nicholas.Stanton",
      email: "Rey.Padberg@rosamond.biz",
      button: '#3'
    },
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "Sincere@april.biz",
      button: '#1'


    },
    {
      id: 2,
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv",
      button: '#2'
    },

    // ... list of items

    {
      id: 11,
      name: "Nicholas DuBuque",
      username: "Nicholas.Stanton",
      email: "Rey.Padberg@rosamond.biz",
      button: '#3'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  onCustom($event) {
    console.log($event.data.id);
    console.log($event.action);
    console.log($event.value);
    
  }
}
