import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'app-my-login',
  templateUrl: './my-login.component.html',
  styleUrls: ['./my-login.component.scss']
})
export class MyLoginComponent implements OnInit {
  renderValue: string;
  @Input() value : number | string;
  @Input() rowData: any;
  constructor() { }

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
    console.log(this);
  }
  click() {
    console.log('clicked button');
    console.log(this.rowData['name']);
    
  }

}
