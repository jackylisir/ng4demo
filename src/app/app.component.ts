import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(
    private router: Router,
    private cookie: CookieService,
  ) { }
  ngOnInit() : void {
    //TODO: need check valid of token here
    if(this.cookie.get('token') != null) {
      this.router.navigate(['/index/dashboard']);
    } else {
      this.router.navigate(['/auth/login']);
    }
    console.log('init app component');
  }
}
