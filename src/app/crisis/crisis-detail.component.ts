import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import 'rxjs/add/operator/switchMap';
import { Apple } from "./apple";

@Component({
    selector:'crisis-detail',
    templateUrl:'./crisis-detail.html',
    styleUrls:['crisis-detail.scss']
    // template:`<p>view1</p>`
})
export class CrisisDetailComponent implements OnInit {
    private apple: Apple;
    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}
    ngOnInit():void {
        // this.route.params.switchMap((params: Params) => params['id'])
        this.apple = new Apple('apple', 10);
        this.route.params.switchMap((params: Params) =>{  
            if(params['id'] == 'abc') {
                return [new Apple('aa', 2)];
            } else {
                return [new Apple('bbb', 3)];
            }
        }).subscribe((fruit:Apple) => {
            console.log(fruit.address);
        });
    }
    public onClick():void {
        this.router.navigate(['/test/view1/hello', {parm1:3, param2:5}]);
    }
}