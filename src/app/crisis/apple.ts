export class Apple {
    constructor(
        public address: string,
        public weight: number
    ) {}
}