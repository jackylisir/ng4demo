import { Component, OnInit } from '@angular/core';
import { GlobalStateService } from "../../service/global-state.service";

@Component({
  selector: 'index-blank-page',
  templateUrl: './blank-page.component.html',
  styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {

  constructor(private globalStatue: GlobalStateService) { }

  ngOnInit() {
    // this.globalStatue.rightBarBS.next("bbbbbbbb");
    console.log('init blank page');
    
  }

}
