import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
        private router:Router,
        // private authService: AuthService
     ) { }

     canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        //  console.log('check protected' + this.authService.isLoggedIn);
        //  console.log(state.url);
         
         if(state.url == '/common/root') {
             return false;
         }
         return this.checkLogin(state.url);
     }
     checkLogin(url:string) : boolean {
        //  if(this.authService.isLoggedIn) {
        //      return true;
        //  }
        //  this.authService.redirectUrl = url;
        //  this.router.navigate(['/auth/login']);
         return true;
     }
}
