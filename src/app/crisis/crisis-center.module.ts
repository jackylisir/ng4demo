import { NgModule } from "@angular/core";
import { routing } from "./crisis-center.routing";
import { CrisisCenterComponent } from "./crisis-center.component";
import { CrisisDetailComponent} from "./crisis-detail.component";
import { CrisisCenterHomeComponent} from "./crisis-center-home.component";
import { CrisisListComponent } from "./crisis-list.component";
import { AuthGuardService } from "../service/auth-guard.service";
import { AuthService } from "../service/auth.service";


@NgModule({
    imports: [routing],
    declarations:[
        CrisisCenterComponent, 
        CrisisCenterHomeComponent , 
        CrisisDetailComponent,
        CrisisListComponent],
    providers:[AuthGuardService, AuthService]
})
export class CrisisModule {

}
