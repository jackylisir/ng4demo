import {
  Directive,
  ElementRef,
  HostListener,
  ViewChild,
  ContentChild,
  Renderer,
  OnInit
} from '@angular/core';

declare var $: any;

@Directive({ selector: '.menu-item' })
export class MenuItemDirective implements OnInit {
  @ContentChild('subMenu') subMenu;
  constructor(private el: ElementRef, private renderer: Renderer) {
  }
  ngOnInit() {
  }
  @HostListener('click', ["$event"])
  menuClicked($event) {
    this.openOrClose();
  }
  openOrClose() {
    let subMenu = this.subMenu.nativeElement;
    // $(this.el.nativeElement).siblings().filter('.active').removeClass('active');
    // $(this.el.nativeElement).addClass('active');
    if (subMenu.style.display == 'block') {
      $(this.subMenu.nativeElement).css('display','none')
      $(this.el.nativeElement).removeClass('open');
      // subMenu.style.display = 'none';
    } else {
      $(this.subMenu.nativeElement).css('display','block')
      $(this.el.nativeElement).addClass('open');
    }
  }
}