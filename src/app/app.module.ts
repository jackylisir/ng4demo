import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders,  } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule, Http } from "@angular/http";
import { CrisisModule} from "./crisis/crisis-center.module";
import { LoginModule} from "./login/login.module";
import { CookieModule } from "ngx-cookie";

import { AppComponent } from './app.component';
import { AuthGuardService } from "./service/auth-guard.service";
import { AuthService } from "./service/auth.service";
import { IndexModule } from "./index/index.module";
import { ErrorModule } from "./error/error.module";
import { GlobalStateService } from "./service/global-state.service";
import { ExtendedHttpService } from "./service/extended-http.service";


const routes: Routes = [
  {path:'', component: AppComponent},
  {path:'auth', redirectTo:'auth/login', pathMatch:'full'},
  {path:'**', redirectTo:'404', pathMatch:'full'}
];
const appRoutesModule:ModuleWithProviders = RouterModule.forRoot(routes);
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CookieModule.forRoot(),
    IndexModule,
    HttpModule,
    appRoutesModule,
    LoginModule,
    ErrorModule
  ],
  providers: [
    { provide: Http, useClass: ExtendedHttpService },
    GlobalStateService, 
    AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
