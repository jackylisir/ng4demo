import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { MeiyuDataSource } from '../datasource/meiyu-data-source';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-comment-table',
  templateUrl: './comment-table.component.html',
  styleUrls: ['./comment-table.component.scss']
})
export class CommentTableComponent implements OnInit {
  source : MeiyuDataSource;
  busy: any;

  settings = {
    pager: {
      perPage: 10
    },
    actions: {
      edit: false, //as an example
      delete: false,
      add: true,
      custom: [
        { name: 'search', title: `<i class="fa fa-search fa-2x"></i> ` },
        { name: 'delete', title: `<i class="fa fa-remove fa-2x"></i> ` },
        { name: 'edit', title: `<i class="fa fa-edit fa-2x"></i> ` },
      ],
      position: 'right',
    },
    attr: {
      class: 'table table-hover table-striped'
    },
    hideSubHeader: true,
    columns: {
      id: {
        title: '编号',
      },
      content: {
        title: '内容',
        width: '60%',
      },
      star: {
        title: '评分',
      },
      hasPhotos: {
        title: '评论图片',
      },
    }
  };
  constructor(http: Http) {
    this.source = new MeiyuDataSource(http, {
      endPoint: environment.endPoint + '/comments',
      totalKey: 'totalElements',
      dataKey: 'content',
      pagerPageKey: 'page',
      pagerLimitKey: 'size',
      sortFieldKey: 'sort',
    })
  }

  ngOnInit() {
    this.source.loading().subscribe((promise) => {
      this.busy = promise;
    })
  }

  onSearch(keyword) {
    console.log('search keyword : %s', keyword);
    this.source.setFilter([
      { field: 'content', search: keyword },
    ], false);
  }

  onUserChoice(event) {
    console.log(event);
  }

}
