import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onMenuClicked(event) {
    let menu = event.target;
    
    console.log('clicked menu');
    console.log($(menu));
    console.log(menu);
    if($(menu).hasClass('open')) {
      $(menu).removeClass('open active');
    } else {
      $(menu).addClass('open active');
    }
    
  }
  onSubMenuClicked() {
    console.log('clicked sub menu');
  }
  hello() {
    console.log('hello menu');
    
  }

}
