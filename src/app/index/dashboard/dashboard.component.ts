import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  shopViewerSetting: any = {
    front: {
      icon: 'bank',
      title: '12121',
      subTitle: '连锁数量',
    },
    back: [
      {
        icon: 'eye',
        title: '浏览',
        router: '/index/blank'
      },
      {
        icon: 'remove',
        title: '设置',
        router: '/index/blank'
      },
      {
        icon: 'ellipsis-h',
        title: '更多操作',
        router: '/index/blank'
      },
    ]
  }

  setting2: any = {
    front: {
      icon: 'bank',
      title: '12121',
      subTitle: '连锁数量',
    },
    back: [
      {
        icon: 'eye',
        title: '浏览',
        router: '/index/blank'
      },
      {
        icon: 'remove',
        title: '设置',
        router: '/index/blank'
      },
      {
        icon: 'ellipsis-h',
        title: '更多操作',
        router: '/index/blank'
      },
    ]
  }

  constructor() { }

  ngOnInit() {
  }

}
