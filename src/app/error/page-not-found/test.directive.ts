import { 
  Directive, 
  OnInit, 
  Input, 
  HostListener, 
  AfterViewInit,
  ViewChildren,
  ContentChildren,
  QueryList,
  ElementRef,
  HostBinding} from '@angular/core';

// @Directive({
//   selector: '[appConfirm]'
// })
@Directive({
  // selector: `[appConfirm]`
  selector: '.directive-nav'
})
export class TestDirective implements OnInit, AfterViewInit {
  // @Input() appConfirm = () =>{};
  // @Input() confirmMessage = "hello";

  // @HostBinding('attr.role') role = 'button';
  // @HostBinding('class.pressed') isPressed : boolean;
  @ContentChildren("div") divs: QueryList<any>
  constructor(private el: ElementRef) { }

  ngOnInit() {
    console.log('init test directive');
  }
  ngAfterViewInit() {
  }
  @HostListener('mousedown') hasPressed() {
  }
  @HostListener('mouseup') hasReleased() {
    // this.isPressed = false;
  }
  // @HostListener('click', ['$event'])
  // confirmClick() {
  //   const confirmed = window.confirm(this.confirmMessage);
  //   console.log('confirm was', confirmed);
    
  //   if(confirmed) {
  //     this.appConfirm();
  //   }
  // }

}
