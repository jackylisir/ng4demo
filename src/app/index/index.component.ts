import { Component } from "@angular/core";

@Component({
    selector: 'index-app',
    styleUrls: ['./index.component.scss'],
    templateUrl: 'index.component.html',
})

export class IndexComponent {
    selectedIndex : number;

    onTabChange() {
        console.log('table changed : %d', this.selectedIndex);
        
    }



}