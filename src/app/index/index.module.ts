import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { CommonModule, DatePipe } from '@angular/common';
import { NavCollapseDirective } from '../directives/nav-collapse.directive';
import { AngularEchartsModule } from 'ngx-echarts';
import { MaterialModule, 
  MdButtonModule, 
  MdToolbarModule, 
  MdTabsModule,
  MdInputModule,
  MdDialogModule,
  MdAutocompleteModule,
  MdMenuModule, 
  MdIconModule } from '@angular/material';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RightBarComponent } from './right-bar/right-bar.component';
import { HeaderComponent } from './header/header.component';
import { IndexComponent } from "./index.component";
import { BlankPageComponent } from './blank-page/blank-page.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableComponent } from './table/table.component';
import { CustomDemoComponent } from './components/custom-demo/custom-demo.component';
import { MyLoginComponent } from './components/my-login/my-login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardCardComponent } from './components/dashboard-card/dashboard-card.component';
import { CommentTableComponent } from './table/comment-table/comment-table.component';
import { ShopComponent } from './table/shop/shop.component';
import { CityComponent } from './table/city/city.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusyModule, BusyConfig } from 'angular2-busy';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabComponent } from './tab/tab.component';
import { CommentEditComponent } from './dialog/comment-edit/comment-edit.component';
import { DemoComponent } from './charts/demo/demo.component';

const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent,
    children: [
      {
        path: 'blank',
        component: BlankPageComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'comps',
        component: CustomDemoComponent
      },
      {
        path: 'table',
        children: [
          { path: 'index', component: TableComponent },
          { path: 'comment', component: CommentTableComponent },
          { path: 'shop', component: ShopComponent },
          { path: 'city', component: CityComponent },
        ]
      },
      {
        path: 'charts',
        children: [
          {path: 'demo' , component: DemoComponent}
        ]
      },
      { path: 'tab', component: TabComponent},
      { path: '', redirectTo: 'blank', pathMatch: 'full' }
    ]
  },
  // {path: 'index', component: AppComponent},
];

@NgModule({
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    BusyModule,
    MaterialModule,
    MdIconModule,
    MdToolbarModule,
    MdTabsModule,
    MdInputModule,
    MdMenuModule,
    MdAutocompleteModule,
    MdButtonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularEchartsModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [MyLoginComponent, 
    CommentEditComponent
  ],
  declarations: [IndexComponent, 
    SidebarComponent, 
    NavCollapseDirective,
    RightBarComponent, 
    HeaderComponent, 
    IndexComponent, 
    BlankPageComponent, 
    TableComponent, 
    CustomDemoComponent, 
    MyLoginComponent, 
    DashboardComponent, 
    DashboardCardComponent, 
    CommentTableComponent, 
    ShopComponent, 
    CityComponent, 
    DemoComponent,
    TabComponent, CommentEditComponent],

  providers: [DatePipe, 
    { provide: BusyConfig, useFactory: busyConfigFactory }
  ]
})
export class IndexModule { }

export function busyConfigFactory() {
  return new BusyConfig({
      message: 'Don\'t panic!',
      backdrop: false,
      // template: '<div class="spinner"></div>',
      template: '<div class="sk-fading-circle"> <div class="sk-circle1 sk-circle"></div> <div class="sk-circle2 sk-circle"></div> <div class="sk-circle3 sk-circle"></div> <div class="sk-circle4 sk-circle"></div> <div class="sk-circle5 sk-circle"></div> <div class="sk-circle6 sk-circle"></div> <div class="sk-circle7 sk-circle"></div> <div class="sk-circle8 sk-circle"></div> <div class="sk-circle9 sk-circle"></div> <div class="sk-circle10 sk-circle"></div> <div class="sk-circle11 sk-circle"></div> <div class="sk-circle12 sk-circle"></div> </div>',
      delay: 200,
      minDuration: 600,
      wrapperClass: 'global-indicator'
  });
}